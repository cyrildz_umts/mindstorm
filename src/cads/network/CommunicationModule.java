package cads.network;
import cads.network.Message;
import java.util.concurrent.BlockingQueue;
import java.net.InetAddress;

public interface CommunicationModule {
	static final int NUL = 0x00;
	static final int SOH = 0x01;
	static final int STX = 0x02;
	static final int ETX = 0x03;
	static final int EOT = 0x04;
	static final int ENQ = 0x05;
	static final int ACK = 0x06;
	static final int EM  = 0x19;
	
	public void init();
	public void setQueue(BlockingQueue<String> queue);
	/**
	 * Add str into the Queue
	 * @param str the string to be added
	 * This method is asynchronous
	 * 
	 */
	public void push(String str);
	public void pop(String str);
	public void createConnection();
	public void removeConnection();
	public void sendData(byte[] data);
	public byte[] getData();
	public boolean isvlaid();

}
