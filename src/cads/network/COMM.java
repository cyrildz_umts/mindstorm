package cads.network;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
//import java.util.concurrent.BlockingQueue;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;


public class COMM implements ICOM {

	private Socket connection;
	private Observer observer;
	private List<Observable> observees;
	private BlockingQueue<String> queue;
	
	public class WriteTask implements Runnable{

		private ICOM comm;
		private boolean quit;
		
		public WriteTask(ICOM comm) {
			this.comm = comm;
			quit = false;
		}
		void setQuit(){
			quit = true;
		}
		

		@Override
		public void run() {
			String str;
			System.out.println("Writer Task started ...");
			try {
				while(!quit){
					if(((COMM)comm).getQueue().size() > 0)
					{
						str = ((COMM)comm).getQueue().take();
						//System.err.println("COMM : data sent : ");
						//System.err.println(str);
						comm.sendDataStream(str);
					}
					
				}
				System.out.println("Writer Task quitting...");
				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
				System.exit(-1);
			}
			
			
		}
		
	}
	
	public class ReaderTask implements Runnable{
		private ICOM comm;
		private boolean quit;
		
		public ReaderTask(ICOM comm) {
			this.comm = comm;
			quit = false;
		}
		
		public void setQuit(){
			quit = true;
		}
		
		@Override
		public void run(){
			//String str;
			
			while(!quit && !Thread.currentThread().isInterrupted()){
				comm.readDataStream();
			}
		}
		
	}
	
	private WriteTask writer;
	private ReaderTask reader;
	
	public COMM(Socket connection) throws NullPointerException{
		if(connection != null){
			this.connection = connection;
			//queue = new ArrayBlockingQueue<>(50);
			observees = new ArrayList<Observable>();
			//startService();
		}
		else throw (new NullPointerException("COMM : connection is null"));
		
	}
	public void stopServices(){
		if(writer != null)
			writer.setQuit();
		if(reader != null)
			reader.setQuit();
	}
	public void startReaderService(){
		reader = new ReaderTask(this);
		Thread rdTask = new Thread(reader);
		rdTask.start();
	}
	public void startWriterService(){
		writer = new WriteTask(this);
		Thread wrTask = new Thread(writer);
		wrTask.start();
		
	}
	public void startService(){
		startWriterService();
		startReaderService();
	}
	@Override
	public void sendNUL() {
		try {
			connection.getOutputStream().write(NUL);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void sendSOH() {
		try {
			connection.getOutputStream().write(SOH);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendSTX() {
		try {
			connection.getOutputStream().write(STX);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendETX() {
		try {
			connection.getOutputStream().write(ETX);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendEOT() {
		try {
			connection.getOutputStream().write(EOT);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendENQ() {
		
		try {
			connection.getOutputStream().write(ENQ);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void sendACK() {
		try {
			connection.getOutputStream().write(ACK);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendEM() {
		try {
			connection.getOutputStream().write(EM);
			connection.getOutputStream().flush();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendDataStream(byte[] data, int count) {
		try {
			connection.getOutputStream().write(data, 0, count);
			//connection.getOutputStream().flush();
			sendETX();
		} catch (IOException e) {
			e.printStackTrace();
		}


	}

	@Override
	public void sendDataStream(byte[] data) {
		this.sendDataStream(data, data.length);

	}

	@Override
	public void sendDataStream(String string) {
		this.sendDataStream(string.getBytes(), string.length());

	}
	@Override
	public void setObserver(Observer observer) {
		this.observer = observer;
		
		
	}
	@Override
	public void removeObserver() {
		this.observer = null;
		
	}
	@Override
	public void addOberservee(Observable observee) {
		
		if(observee != null){
			observee.addObserver(observer);
			observees.add(observee);
		}
	}
	@Override
	public void removeOberservee(Observable observee) {
		if(observees.remove(observee)){
			observee.deleteObserver(observer);
		}
	}
	@Override
	public void readDataStream(List<Byte> data) {
		int c ;
		int count = 0;
		byte[]response;
		String reply;
		boolean isAvailable = true;
		int n = 0;
		try 
		{
			if(isValid())
			{
				
					for(c = connection.getInputStream().read(); (char)c != ETX; c = connection.getInputStream().read())
					{
						data.add((byte)c);
						if(count < 10 ){
							System.out.print(".");
						}
						else{
							System.out.println();
						}
						count++;		
					}
					response = new byte[data.size()];
					for(int i = 0; i < response.length; i++){
						response[i] = data.get(i).byteValue();
					}
					reply = new String(response);
					
					for(Observable ob : observees){
						//((Journal)ob).setContent(reply);
						//ob.setChanged();
						//ob.notifyObservers(reply);
					}
			}			
			

		}catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	
	public BlockingQueue<String> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<String> q) {
		this.queue = q;
	}

	@Override
	public boolean isValid() {
		return (connection != null && connection.isConnected() && !connection.isClosed());
	}
	@Override
	public void readDataStream() {
		int c;
		StringBuilder builder = new StringBuilder();
		//String reply;
		try{
			if(isValid())
			{
				
					for(c = connection.getInputStream().read(); (char)c != ETX; c = connection.getInputStream().read())
					{
						if((((char)c)== EOT) || (c == -1))
						{
							if(writer != null)
								writer.setQuit();
							if (reader != null)
								reader.setQuit();
							System.out.println("The Connection Peer has left ...");
							connection.close();
						}
						else 
						{
							builder.append((char)c);
						}
									
					}
					//System.err.println(this.getClass() + " readDataStream(): ");
					System.err.println("Message received : " + builder.toString());
					if(observer != null)
					{
						observer.update(null, builder.toString());
					}
					
					for(Observable ob : observees){
						//((Journal)ob).setContent( builder.toString());
						//ob.setChanged();
						ob.notifyObservers(builder.toString());
					}
			}		
		}
		catch (Exception e) {
			
		}
			
	}
	@Override
	public void closeConnection() {
		if(connection != null)
		{
			try {
				connection.close();
			} catch (IOException e) {
				System.out.println("Connection closed error : ");
				e.printStackTrace();
			}
		}
		
	}

}
