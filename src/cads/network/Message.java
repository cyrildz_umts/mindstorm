package cads.network;

import org.json.simple.JSONObject;

public class Message {
	private int messageType;

	private int requestID;
	private String remoteReference;
	private int operationID;
	private int retStatus;
	private int percent;
	private boolean value;
	private boolean isGrip;
	private String state;
	private String type;
	private int transactionID;
	private JSONObject json;


	public Message(){

	}
	public Message(JSONObject json)
	{
		this.json = json;
	}
	public Message(int transactionID, String state, String type, boolean value, int percent)
	{
		this.transactionID = transactionID;
		this.state = state;
		this.type = type;
		this.value = value;
		this.percent = percent;
	}

	public JSONObject getJson() {
		return json;
	}
	public void setJson(JSONObject json) {
		this.json = json;
	}

	public boolean isGripp() {
		return isGrip;
	}
	public void setGripp(boolean isGripp) {
		this.isGrip = isGripp;
	}
	@Override
	public String toString()
	{
		return json.toJSONString();
	}
	public static Message buildMessageFromJSON(JSONObject json)
	{
		Message message = new Message();
		System.out.println("entering buildmessage ...");
		String state = (String) json.get("state");
		String type = (String) json.get("type");
		boolean value = false;
		if(state.compareTo("gripper") == 0)
		{
			System.out.println("buildmessage gripper ...");
			if(!((String)json.get("value") == "closed"))
			{
				value = true;
			}
			message.setGripp(value);
		}
		else if( !state.equals("ultrasonic"))
		{
			System.out.println("buildmessage not gripper...");
			System.out.println("Percent : " + json.get("percent"));
			message.setPercent(Integer.parseInt(json.get("percent").toString()));
			message.setGripp(value);
		}
		message.setState(state);
		message.setType(type);
		message.setValue(value);
		System.out.println("leaving buildmessage ...");
		return message;
	}
	public int getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(int transactionID) {
		this.transactionID = transactionID;
	}

	public boolean isGripper()
	{
		return getState().equals("gripper");
	}
	public String getState() {
		return (String) json.get("state");
	}
	public void setState(String state) {
		this.state = state;
		json.put("state", state);
	}
	public String getType() {
		return (String) json.get("type");
	}
	public void setType(String type) {
		this.type = type;
		json.put("type", type);
	}
	public int getPercent() {
		return Integer.parseInt(json.get("percent").toString());
	}
	public void setPercent(int percent) {
		this.percent = percent;
		json.put("percent", percent);
	}
	public boolean isValue() {
		return ((String) json.get("value")).equals("closed");
	}
	public void setValue(boolean value) {
		this.value = value;
		if(value)
		{
			json.put("value", "closed");
		}
		else
		{
			json.put("value", "opened");
		}
	}
	public int getMessageType() {
		return messageType;
	}
	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}
	public int getRequestID() {
		return requestID;
	}
	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}
	public String getRemoteReference() {
		return remoteReference;
	}
	public void setRemoteReference(String remoteReference) {
		this.remoteReference = remoteReference;
	}
	public int getOperationID() {
		return operationID;
	}
	public void setOperationID(int operationID) {
		this.operationID = operationID;
	}
	public int getRetStatus() {
		return retStatus;
	}
	public void setRetStatus(int retStatus) {
		this.retStatus = retStatus;
	}

}
