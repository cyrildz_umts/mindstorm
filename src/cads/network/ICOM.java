package cads.network;

import java.util.Observer;
import java.util.List;
import java.util.Observable;

public interface ICOM {

	static final int NUL = 0x00;
	static final int SOH = 0x01;
	static final int STX = 0x02;
	static final int ETX = 0x03;
	static final int EOT = 0x04;
	static final int ENQ = 0x05;
	static final int ACK = 0x06;
	static final int EM  = 0x19;
	
	/**
	 * defines an observer object to be notify when
	 * a new message has been received.
	 * @param observer The observer to be notified
	 */
	public void setObserver(Observer observer);
	/**
	 * stops sending notification to the observer.
	 */
	public void removeObserver();
	/**
	 * add an observee to the list of observable objects.
	 * @param observee
	 */
	public void addOberservee(Observable observee);
	/**
	 * Remove an observee from the list of Observable objects.
	 * @param observee 
	 */
	public void removeOberservee(Observable observee);
	
	/**
	 * send NUL(null character) Code signal to the other end
	 */
	public void sendNUL();
	/**
	 * sends SOH(Start oh Header) to the other end
	 */
	public void sendSOH();
	/**
	 * send STX (Start of Text) to the other end of the connection
	 */
	public void sendSTX();
	/**
	 * send ETX(End Of Text) to the end of the connection
	 */
	public void sendETX();
	/**
	 * send EOT(End Of Transmission) to the other end of the connection
	 */
	public void sendEOT();
	/**
	 * send ENQ(Enquiry) to the other end of the connection.
	 */
	public void sendENQ();
	/**
	 * send ACK(Acknowledge) to the other end of the connection
	 */
	public void sendACK();
	/**
	 * send EM(End of Medium) to the other end the connection
	 */
	public void sendEM();
	/**
	 * sends data through the communication channel
	 * @param data the byte stream to be sent over 
	 * @param count How many byte from data should be sent
	 */
	public void sendDataStream(byte[] data, int count);
	/**
	 * sends data through the communication channel
	 * @param data the byte stream to be sent over 
	 */
	public void sendDataStream(byte[] data);
	/**
	 * send a string through the communication channel
	 * @param string
	 */
	public void sendDataStream(String string);
	/**
	 * read data stream from the communication channel
	 * @param data buffer where the received stream is saved.
	 */
	public void readDataStream(List<Byte> data);
	/**
	 * 
	 */
	public void readDataStream();
	/**
	 * 
	 * @return
	 */
	public boolean isValid();
	/**
	 * 
	 */
	public void closeConnection();
}
