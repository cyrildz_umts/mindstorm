package cads.test.junit.simulation;

import static org.junit.Assert.*;

import org.cads.ev3.middleware.CaDSEV3RobotStudentImplementation;
import org.cads.ev3.middleware.CaDSEV3RobotType;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotFeedBackListener;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotStatusListener;
import org.cads.ev3.simulation.CaDSEV3RobotGUI;
import org.json.simple.JSONObject;
import org.junit.Test;

import lejos.utility.Delay;

public class TestProviderLeftRight {
    private static CaDSEV3RobotStudentImplementation caller = null;
    private CaDSEV3RobotGUI robot;

    private class TestListener implements Runnable, ICaDSEV3RobotStatusListener, ICaDSEV3RobotFeedBackListener {

        @Override
        public synchronized void giveFeedbackByJSonTo(JSONObject feedback) {
        	System.out.print("Feedback ");
            System.out.println(feedback);
        }

        @Override
        public synchronized void onStatusMessage(JSONObject status) {
        	System.out.println("Status Message ");
            System.out.println(status);

        }

        @Override
        public void run() {
            // TODO Auto-generated method stub
        	JSONObject json = new JSONObject();
        	
            try {
                caller = CaDSEV3RobotStudentImplementation.createInstance(CaDSEV3RobotType.SIMULATION, this, this);
                //robot = new CaDSEV3RobotGUI();
                //robot = CaDSEV3RobotGUI.getGUI();
                boolean on = true;
                //caller.moveLeft();
                //robot.doStepLeft();
                //robot.doClose();
                
                
                while (!Thread.currentThread().isInterrupted()) {                	
                	
                    if (on) {
                        caller.stop_h(); // stops any movement
                        caller.moveLeft();
                        System.out.println("Moving left ...");
                        caller.doOpen();;
                        
                        
                    } else {
                        caller.stop_h(); // stops any movement
                        caller.moveRight();
                        caller.doClose();
                    }
                    on = !on;
                    Delay.msDelay(5100);

                }
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(-1);
            }
            System.exit(0);
        } 
    }

    @Test
    public void test() {
        try {
            TestListener l = new TestListener();
            (new Thread(l)).start();
            waithere();
            System.exit(0);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Not yet implemented");
        }
    }

    synchronized public void waithere() {
        try {
            this.wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
