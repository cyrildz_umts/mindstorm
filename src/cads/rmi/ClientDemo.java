package cads.rmi;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.cads.ev3.gui.swing.CaDSRobotGUISwing;

import cads.network.COMM;
import cads.network.ICOM;

public class ClientDemo {
	
	
	
	public static void main(String[] args) throws UnknownHostException, IOException{
		
		GuiService service = new GuiService();
		String host = "localhost";
		int port = 8090;
		Socket connection = new Socket(host, port);
		final int CAPICITY = 50;
		BlockingQueue<String> queue = new ArrayBlockingQueue<String>(CAPICITY);
		String moduleName = "RemoteRef";
		IRemoteModuleRef remoteRef = new RemoteModule(moduleName);
		IRemoteProxy proxy = new Proxy(remoteRef);
		((Proxy)proxy).setQueue(queue);
		ICOM comm = new COMM(connection);
		((COMM)comm).setQueue(queue);
		((COMM)comm).startService();
		comm.setObserver((Proxy)proxy);
		remoteRef.setProxy(proxy);
		CaDSRobotGUISwing gui = new CaDSRobotGUISwing(remoteRef, remoteRef, remoteRef, remoteRef, remoteRef);
		((RemoteModule)remoteRef).setGui(gui);
		gui.addService("Service 1");
		service.waithere();
	}

}
