package cads.rmi;

import java.util.concurrent.BlockingQueue;

import org.cads.ev3.middleware.CaDSEV3RobotStudentImplementation;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotFeedBackListener;
import org.cads.ev3.middleware.hal.ICaDSEV3RobotStatusListener;
import org.json.simple.JSONObject;

import cads.network.Message;

public class RequestListener implements ICaDSEV3RobotStatusListener, ICaDSEV3RobotFeedBackListener {
	private BlockingQueue<String> queue;
	private int limit;
	
	
	public BlockingQueue<String> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<String> queue) {
		this.queue = queue;
	}

	@Override
	public synchronized void giveFeedbackByJSonTo(JSONObject feedback) {
		System.out.println("Listener  giveFeedBack called ...: " + feedback);
		//queue.put(feedback.toJSONString());
		Message message = new Message(feedback);
		if(!message.isGripper() && !message.getState().equals("ultrasonic"))
		{
			if(message.getPercent() == limit)
			{
				if(message.getState().equals("vertical"))
				{
					CaDSEV3RobotStudentImplementation.getInstance().stop_v();
				}
				else
				{
					CaDSEV3RobotStudentImplementation.getInstance().stop_h();
				}
			}
		}
		
	}

	@Override
	public synchronized void onStatusMessage(JSONObject status) {
		
		try {
			//System.out.println("Listener  onStatus called ...");
			queue.put(status.toJSONString());
			Message message = new Message(status);
			if(!message.isGripper() && !message.getState().equals("ultrasonic"))
			{
				if(message.getPercent() == limit)
				{
					if(message.getState().equals("vertical"))
					{
						CaDSEV3RobotStudentImplementation.getInstance().stop_v();
					}
					else
					{
						CaDSEV3RobotStudentImplementation.getInstance().stop_h();
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
