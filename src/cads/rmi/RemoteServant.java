package cads.rmi;

import java.util.Observable;
import java.util.Observer;

import org.cads.ev3.gui.ICaDSRobotGUIUpdater;
import org.cads.ev3.middleware.CaDSEV3RobotStudentImplementation;
import org.cads.ev3.middleware.CaDSEV3RobotType;
import org.cads.ev3.rmi.generated.cadSRMIInterface.IIDLCaDSEV3RMIMoveHorizontal;
import org.cads.ev3.rmi.provider.MyRobotMiddlewareImplementationMoveHorizontal;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import cads.network.Message;

public class RemoteServant implements IRemoteModuleRef, Observer {

	private String name;
	/**
	 * MIDDLE : help us to decide whether the robot should move left or Right
	 * Percent > MIDDLE --> Move to the right
	 * Percent < MIDDLE --> Move to the left
	 */
	private final int MIDDLE = 50;
	IIDLCaDSEV3RMIMoveHorizontal horizontal;
	private int lastVerticalPercent;
	private int lastHorizontalPercent;
	private boolean gripperClosed;
	private boolean busy;
	private static CaDSEV3RobotStudentImplementation caller = null;
	private RequestListener listener;
	
	
	public RemoteServant(RequestListener listener) {
		this.listener = listener;
		busy = false;
		caller = CaDSEV3RobotStudentImplementation.createInstance(CaDSEV3RobotType.SIMULATION, listener, listener);
	}
	public RemoteServant(CaDSEV3RobotType type, RequestListener listener) {
		this.listener = listener;
		busy = false;
		try {
			horizontal = MyRobotMiddlewareImplementationMoveHorizontal.class.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		caller = CaDSEV3RobotStudentImplementation.createInstance(type, listener, listener);
	}
	@Override
	public int closeGripper(int transactionID) throws Exception {
		caller.doClose();
		gripperClosed = true;
		return 0;
	}

	@Override
	public int isGripperClosed() throws Exception {
		
		return gripperClosed ? 1 : 0;
	}

	@Override
	public int openGripper(int transactionID) throws Exception {

		caller.doOpen();
		
		return 0;
	}

	@Override
	public int getCurrentHorizontalPercent() throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int moveHorizontalToPercent(int transactionID, int percent) throws Exception {
		System.out.println("servant moveHorizontal called (percent  : " + percent + " ) ...");
		listener.setLimit(percent);
		busy = !busy;
		if(percent > MIDDLE)
		{

			caller.moveRight();
		}
		else if( percent < MIDDLE)
		{

			caller.moveLeft();
		}
		busy = !busy;
		return 0;
	}

	@Override
	public int stop(int transactionID) throws Exception {
		caller.stop_v();
		caller.stop_h();
		return 0;
	}

	@Override
	public int getCurrentVerticalPercent() throws Exception {
		
		return 0;
	}

	@Override
	public int moveVerticalToPercent(int transactionID, int percent) throws Exception {
		listener.setLimit(percent);
		System.out.println("servant moveVertical called (percent  : " + percent + " ) ...");
		busy = !busy;
		if(percent > MIDDLE)
		{
			caller.moveUp();
		}
		else if( percent < MIDDLE)
		{

			caller.moveDown();
		}
		busy = !busy;
		return 0;
	}

	@Override
	public int isUltraSonicOccupied() throws Exception {
		
		return busy ? 1 : 0;
	}

	@Override
	public void register(ICaDSRobotGUIUpdater arg0) {
		

	}

	@Override
	public void update(String arg0) {
		

	}

	@Override
	public void setProxy(IRemoteProxy proxy) {
		

	}

	@Override
	public String getRefName() {
		
		return name;
	}

	@Override
	public void setRefName(String name) {
		this.name = name;

	}

	@Override
	public void updateGUI(Message message) {
		System.out.println("Udating the Servant GUI");
		String state = message.getState();
		if(state.equals("gripper"))
		{
			if(message.isValue())
			{
				caller.doClose();
			}
			else
			{
				caller.doOpen();
			}
		}
		
		else if(state.equals("vertical"))
		{
			int  percent = message.getPercent(); 
			caller.giveFeedbackByJSonTo(message.getJson());
			try {
				moveVerticalToPercent(0, percent);
				//caller.giveFeedbackByJSonTo(message.getJson());
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		else if(state.equals("horizontal"))
		{
			int  percent = message.getPercent(); 
			//caller.giveFeedbackByJSonTo(message.getJson());
			try {
				
				moveHorizontalToPercent(0, percent);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			System.err.println("Remoteservant Unknown command ...");
		}
		
		System.out.println("Udating the Servant GUI finished ");
	}
	@Override
	public void update(Observable o, Object arg) {
		System.err.println("Remoteservant Observer : new message received :" + (String)arg );
		JSONObject json;
		try {
			json = (JSONObject) (new JSONParser()).parse((String) arg);
			Message message = new Message(json);
			updateGUI(message);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

}
