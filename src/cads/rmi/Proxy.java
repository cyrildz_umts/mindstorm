package cads.rmi;

import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.BlockingQueue;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import cads.network.*;

public class Proxy implements IRemoteProxy, Observer {
	private IRemoteModuleRef remote;
	private BlockingQueue<String> queue;
	
	public BlockingQueue<String> getQueue() {
		return queue;
	}

	public void setQueue(BlockingQueue<String> queue) {
		this.queue = queue;
	}

	public Proxy(IRemoteModuleRef remote) throws NullPointerException{
		if(remote != null)
		{
			this.remote = remote;
			this.queue = null;
		}
		else throw new NullPointerException();
	}

	@Override
	public String marsharl(JSONObject json) {
		
		return json == null ? null : json.toJSONString();
	}

	@Override
	public JSONObject unmarsharl(String data) throws ParseException {
		return data == null ? null : (JSONObject)(new JSONParser()).parse(data);
	}

	@Override
	public JSONObject unmarsharl(byte[] data) throws ParseException{
		return data == null ? null : (JSONObject)(new JSONParser()).parse(new String(data));
	}

	@Override
	public void send(String data) {
		try {
			queue.put(data);
			//System.out.println("Data content : " + data);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void updateRobot(String reply) {
		
		
	}

	@Override
	public void sendMessage(Message message) {
		send(message.toString());
		
	}

	@Override
	public synchronized void update(Observable o, Object arg) {
		try {
			System.out.println("Proxy UPDATE ::::");
			JSONObject jsonReply = unmarsharl((String)arg);
			if(!jsonReply.isEmpty())
			{
				Message message = new Message(jsonReply);
				System.out.println("message :" + message);
				remote.updateGUI(message);
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
}
