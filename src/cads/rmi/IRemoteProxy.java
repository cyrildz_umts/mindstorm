package cads.rmi;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import cads.network.Message;

public interface IRemoteProxy {
	public String marsharl(JSONObject json);
	public JSONObject unmarsharl(String data)throws ParseException;
	public JSONObject unmarsharl(byte[] data)throws ParseException;
	public void sendMessage(Message message);
	public void send(String data);
	public void updateRobot(String reply);

}
