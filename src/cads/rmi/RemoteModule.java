package cads.rmi;

import org.cads.ev3.gui.ICaDSRobotGUIUpdater;
import org.cads.ev3.gui.swing.CaDSRobotGUISwing;
import org.json.simple.JSONObject;

import cads.network.Message;

//import cads.rmi.Message.MessageTyp;
//import cads.rmi.Message.OperationTyp;

public class RemoteModule implements IRemoteModuleRef {
	private String name;
	
	private IRemoteProxy proxy;
	private int lastVerticalPercent;
	private int lastHorizontalPercent;
	private boolean gripperClosed;
	ICaDSRobotGUIUpdater gui;
	
	public RemoteModule(String name) {
		this.name = name;
		this.proxy = null;
	}
	
	
	public int getLastVerticalPercent() {
		return lastVerticalPercent;
	}

	public void setLastVerticalPercent(int lastVerticalPercent) {
		this.lastVerticalPercent = lastVerticalPercent;
	}

	public int getLastHorizontalPercent() {
		return lastHorizontalPercent;
	}

	public void setLastHorizontalPercent(int lastHorizontalPercent) {
		this.lastHorizontalPercent = lastHorizontalPercent;
	}

	public ICaDSRobotGUIUpdater getGui() {
		return gui;
	}

	public void setGui(ICaDSRobotGUIUpdater gui) {
		this.gui = gui;
	}

	public void setGripperClosed(boolean gripperClosed) {
		this.gripperClosed = gripperClosed;
	}

	

	private Message buildRemoteCallMessage(int transactionID, String state, String type, boolean value,
			int percent){
		JSONObject json = new JSONObject();
		json.put("transactionID", transactionID);
		json.put("state", state);
		json.put("type", type);
		if(percent < 0)
		{
			json.put("value", value ? "closed": "opened");
		}
		else
		{
			json.put("percent", percent);
		}
		Message message = new Message(json);
		return message;
	}
	@Override
	public int closeGripper(int transactionID) throws Exception {
		if(proxy == null)
		{
			throw new NullPointerException();
		}
		Message message = buildRemoteCallMessage(transactionID, "gripper","GRIPPER_INFO",true ,-1);
		//System.out.println(message);
		proxy.sendMessage(message);
		
		return 0;
	}

	@Override
	public int isGripperClosed() throws Exception {
		if(proxy == null)
		{
			throw new NullPointerException();
		}
		
		return gripperClosed ? 0 : 1;
	}

	@Override
	public int openGripper(int transactionID) throws Exception {
		Message message = buildRemoteCallMessage(transactionID, "gripper","GRIPPER_INFO", false,-1);
		//System.out.println(message);
		proxy.sendMessage(message);
		return 0;
	}

	@Override
	public int getCurrentHorizontalPercent() throws Exception {
		
		
		return lastHorizontalPercent;
	}

	@Override
	public int moveHorizontalToPercent(int transactionID, int percent) throws Exception {
		Message message = buildRemoteCallMessage(transactionID, 
				"horizontal",
				"GRIPPER_INFO",
				false ,
				percent);
		//System.out.println(message);
		proxy.sendMessage(message);
		
		return 0;
	}

	@Override
	//TODO
	public int stop(int transactionID) throws Exception {
		Message message = buildRemoteCallMessage(transactionID, "gripper","GRIPPER_INFO",false ,-1);
		//System.out.println(message);
		proxy.sendMessage(message);
		return 0;
	}

	@Override
	public int getCurrentVerticalPercent() throws Exception {
		
		return lastVerticalPercent;
	}

	@Override
	public int moveVerticalToPercent(int transactionID, int percent) throws Exception {
		Message message = buildRemoteCallMessage(transactionID, "vertical","GRIPPER_INFO",false ,percent);
		//System.out.println(message);
		proxy.sendMessage(message);
		return 0;
	}

	@Override
	public int isUltraSonicOccupied() throws Exception {
		Message message = buildRemoteCallMessage(0, "ultrasonic","GRIPPER_INFO",false ,-1);
		//System.out.println(message);
		proxy.sendMessage(message);
		return 0;
	}

	@Override
	public void register(ICaDSRobotGUIUpdater gui) {
		System.out.println("New Observer");
        gui.addService("Service 1");
        gui.addService("Service 2");
        gui.setChoosenService("Service 1", -1, -1, false);

	}

	@Override
	public void update(String jsonString) {
		System.out.println("new message ... " + jsonString);

	}

	@Override
	public void setProxy(IRemoteProxy proxy) {
		//if(proxy != null)
			this.proxy = proxy;

	}

	@Override
	public String getRefName() {
		
		return name;
	}

	@Override
	public void setRefName(String name) {
		this.name = name;
		
	}

	private void processFeedBack(Message message){
		
	}
	
	private void processStatus(Message message){
		
	}
	
	private void processError(Message message){
		
	}
	
	@Override
	public void updateGUI(Message message) {
		if(message.isGripper()){
			if(message.isValue())
			{
				gui.setGripperClosed();
			}
			else
			{
				gui.setGripperOpen();
			}
		}
		else if(message.getState().equals("vertical")){
			lastVerticalPercent = message.getPercent();
			gui.setVerticalProgressbar(lastVerticalPercent);
		}
		else if(message.getState().equals("horizontal")){
			lastHorizontalPercent = message.getPercent();
			gui.setHorizontalProgressbar(lastHorizontalPercent);
			
		}
		
		System.out.println("GUI UPDATED : " + message );
	}




}
