package cads.rmi;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import cads.network.COMM;
import cads.network.ICOM;

public class ServantDemo {

	public static void main(String[] args) {
		int capacity = 50;
		int port = 8090;
		GuiService service = new GuiService();
		
		BlockingQueue<String> queue = new ArrayBlockingQueue<>(capacity);
		RequestListener listener = new RequestListener();
		listener.setQueue(queue);
		try {
			ServerSocket server = new ServerSocket(port);
			System.out.println("Robot Servant started !");
			System.out.println("waiting for connection from client ...");
			Socket connection = server.accept();
			ICOM comm = new COMM(connection);
			((COMM)comm).setQueue(queue);
			IRemoteModuleRef servant = new RemoteServant(listener);
			servant.setRefName("Robot");
			comm.setObserver((RemoteServant)servant);
			((COMM)comm).startService();
			service.waithere();
			server.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Robot Servant leaving !");		

	}

}
